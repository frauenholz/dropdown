/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    Markdown,
    affects,
    collection,
    editor,
    isFilledString,
    markdownifyToString,
    pgettext,
    tripetto,
} from "tripetto";
import { Dropdown } from "..";
import { DropdownOption } from "../option";

/** Assets */
import ICON from "../../../assets/icon.svg";
import ICON_NOTHING from "../../../assets/nothing.svg";

@tripetto({
    type: "condition",
    context: PACKAGE_NAME,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:dropdown", "Dropdown option");
    },
})
export class DropdownCondition extends ConditionBlock {
    @affects("#name")
    @affects("#condition")
    @collection("#options")
    option: DropdownOption | undefined;

    get name() {
        return (
            (this.option
                ? this.option.name
                : pgettext("block:dropdown", "Nothing selected")) ||
            this.type.label
        );
    }

    get icon() {
        return this.option ? ICON : ICON_NOTHING;
    }

    get options() {
        return (
            (this.node &&
                this.node.block instanceof Dropdown &&
                this.node.block.options) ||
            undefined
        );
    }

    @editor
    defineEditor(): void {
        if (this.node && this.options) {
            const options: Forms.IDropdownOption<DropdownOption | undefined>[] =
                [];

            this.options.each((option: DropdownOption) => {
                if (isFilledString(option.name)) {
                    options.push({
                        label: markdownifyToString(
                            option.name,
                            Markdown.MarkdownFeatures.None
                        ),
                        value: option,
                    });
                }
            });

            options.push({
                label: pgettext("block:dropdown", "Nothing selected"),
                value: undefined,
            });

            this.editor.form({
                title: this.node.label,
                controls: [
                    new Forms.Dropdown<DropdownOption | undefined>(
                        options,
                        Forms.Dropdown.bind(this, "option", undefined)
                    ),
                ],
            });
        }
    }
}
