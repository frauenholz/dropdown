/** Dependencies */
import {
    NodeBlock,
    Slots,
    arrayItem,
    assert,
    findFirst,
    markdownifyToString,
} from "tripetto-runner-foundation";
import { IDropdownOption } from "./option";

export abstract class Dropdown extends NodeBlock<{
    readonly options?: IDropdownOption[];
    readonly randomize?: boolean;
}> {
    /** Contains the randomized options order. */
    private randomized?: {
        readonly index: number;
        readonly id: string;
    }[];

    /** Contains the score slot. */
    readonly scoreSlot = this.valueOf<number, Slots.Numeric>(
        "score",
        "feature"
    );

    /** Contains the dropdown slot. */
    readonly dropdownSlot = assert(
        this.valueOf<string, Slots.String>("option", "static", {
            confirm: true,
            modifier: (data) => {
                if (data.value) {
                    if (!data.reference) {
                        const selected =
                            findFirst(
                                this.props.options,
                                (option) => option.value === data.value
                            ) ||
                            findFirst(
                                this.props.options,
                                (option) => option.id === data.value
                            ) ||
                            findFirst(
                                this.props.options,
                                (option) => option.name === data.value
                            ) ||
                            findFirst(
                                this.props.options,
                                (option) =>
                                    option.name.toLowerCase() ===
                                    data.value.toLowerCase()
                            );

                        return {
                            value:
                                selected && (selected.value || selected.name),
                            reference: selected && selected.id,
                        };
                    } else if (
                        !findFirst(
                            this.props.options,
                            (option) => option.id === data.reference
                        )
                    ) {
                        return {
                            value: undefined,
                            reference: undefined,
                        };
                    }
                }
            },
            onChange: (slot) => {
                if (this.scoreSlot) {
                    const selected = findFirst(
                        this.props.options,
                        (option) => option.id === slot.reference
                    );

                    this.scoreSlot.set(selected && (selected?.score || 0));
                }
            },
        })
    );

    /** Contains if the dropdown is required. */
    readonly required = this.dropdownSlot.slot.required || false;

    /** Retrieves the dropdown options. */
    get options(): IDropdownOption[] {
        const options =
            this.props.options?.map((option) => ({
                ...option,
                name: markdownifyToString(option.name, this.context),
            })) || [];

        if (this.props.randomize && options.length > 1) {
            if (
                !this.randomized ||
                this.randomized.length !== options.length ||
                findFirst(
                    this.randomized,
                    (option) => options[option.index]?.id !== option.id
                )
            ) {
                this.randomized = options.map((option, index) => ({
                    index,
                    id: option.id,
                }));

                let length = this.randomized.length;

                while (--length) {
                    const index = Math.floor(Math.random() * length);
                    const temp = this.randomized[length];

                    this.randomized[length] = this.randomized[index];
                    this.randomized[index] = temp;
                }
            }

            return this.randomized.map((option) => options[option.index]);
        } else if (this.randomized) {
            this.randomized = undefined;
        }

        return options;
    }

    /** Retrieves the reference of the selection option. */
    get value() {
        const selected = findFirst(
            this.props.options,
            (option) => option.id === this.dropdownSlot.reference
        );

        if (!selected && !this.node.placeholder) {
            const defaultOption = arrayItem(this.props.options, 0);

            this.dropdownSlot.default(
                defaultOption && (defaultOption.value || defaultOption.name),
                defaultOption && defaultOption.id
            );

            return (defaultOption && defaultOption.id) || "";
        }

        return (selected && selected.id) || "";
    }

    /** Sets the reference of the selected option. */
    set value(reference: string) {
        this.select(
            findFirst(this.props.options, (option) => option.id === reference)
        );
    }

    /** Selects an option. */
    select(option: IDropdownOption | undefined): string {
        this.dropdownSlot.set(
            option && (option.value || option.name),
            option && option.id
        );

        return (option && option.id) || "";
    }
}
