# Tripetto Dropdown Block
# Copyright (C) 2021 Tripetto B.V.
#
msgid ""
msgstr ""
"Project-Id-Version: tripetto-block-dropdown 4.2.0\n"
"Report-Msgid-Bugs-To: support@tripetto.com\n"
"POT-Creation-Date: 2021-04-06 22:58+0200\n"
"PO-Revision-Date: 2021-04-06 22:59+0200\n"
"Last-Translator: Hisam A Fahri\n"
"Language-Team: \n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 2.4.2\n"

msgctxt "block:dropdown"
msgid "%2 (%1 option)"
msgid_plural "%2 (%1 options)"
msgstr[0] "%2 (%1 pilihan)"

msgctxt "block:dropdown"
msgid "And"
msgstr "Dan"

msgctxt "block:dropdown"
msgid "Click the + button to add an option..."
msgstr "Klik tombol + untuk menambahkan pilihan."

msgctxt "block:dropdown"
msgid "Compare mode"
msgstr "Mode perbandingan"

# There is no proper Indonesian word for 'dropdown'
msgctxt "block:dropdown"
msgid "Dropdown"
msgstr "Dropdown"

msgctxt "block:dropdown"
msgid "Dropdown option"
msgstr "Pilihan dropdown"

msgctxt "block:dropdown"
msgid "Dropdown options"
msgstr "Pilihan dropdown"

msgctxt "block:dropdown"
msgid ""
"Generates a score based on the selected option. Open the settings panel for "
"each option to set the score."
msgstr ""
"Menghasilkan skor berdasarkan pilihan yang dipilih. Buka panel pengaturan "
"untuk setiap pilihan untuk mengatur skor."

msgctxt "block:dropdown"
msgid "Identifier"
msgstr "Pengenal"

msgctxt "block:dropdown"
msgid ""
"If an option identifier is set, this identifier will be used as selected "
"option value instead of the option label."
msgstr ""
"Jika pengenal opsi disetel, pengenal ini akan digunakan sebagai nilai opsi "
"yang dipilih alih-alih label opsi."

msgctxt "block:dropdown"
msgid "If score equals"
msgstr "Jika nilai sama dengan"

msgctxt "block:dropdown"
msgid "If score is between"
msgstr "Jika nilai di antara"

msgctxt "block:dropdown"
msgid "If score is higher than"
msgstr "Jika nilai lebih besar dari"

msgctxt "block:dropdown"
msgid "If score is lower than"
msgstr "Jika nilai lebih kecil dari"

msgctxt "block:dropdown"
msgid "If score is not between"
msgstr "Jika nilai tidak di antara"

msgctxt "block:dropdown"
msgid "If score not equals"
msgstr "Jika nilai tidak sama dengan"

msgctxt "block:dropdown"
msgid "Name"
msgstr "Nama"

msgctxt "block:dropdown"
msgid "Nothing selected"
msgstr "Tidak ada yang dipilih"

msgctxt "block:dropdown"
msgid "Number"
msgstr "Angka"

msgctxt "block:dropdown"
msgid "Option name"
msgstr "Nama pilihan"

msgctxt "block:dropdown"
msgid "Options"
msgstr "Pilihan"

msgctxt "block:dropdown"
msgid "Score"
msgstr "Skor"

msgctxt "block:dropdown"
msgid "Score is between"
msgstr "Nilai di antara"

msgctxt "block:dropdown"
msgid "Score is calculated"
msgstr "Nilai sedang dihitung"

msgctxt "block:dropdown"
msgid "Score is equal to"
msgstr "Nilai sama dengan"

msgctxt "block:dropdown"
msgid "Score is higher than"
msgstr "Nilai lebih besar dari"

msgctxt "block:dropdown"
msgid "Score is lower than"
msgstr "Niai lebih kecil dari"

msgctxt "block:dropdown"
msgid "Score is not between"
msgstr "Nilai tidak di antara"

msgctxt "block:dropdown"
msgid "Score is not calculated"
msgstr "Nilai tidak dihitung"

msgctxt "block:dropdown"
msgid "Score is not equal to"
msgstr "Nilai tidak sama dengan"

msgctxt "block:dropdown"
msgid "Selected option"
msgstr "Pilihan yang dipilih"

msgctxt "block:dropdown"
msgid "Unnamed option"
msgstr "Pilihan tanpa nama"

msgctxt "block:dropdown"
msgid "Use fixed number"
msgstr "Gunakan bilangan tetap"

msgctxt "block:dropdown"
msgid "Use value of"
msgstr "Gunakan nilai dari"

msgctxt "block:dropdown"
msgid "Value"
msgstr "Nilai"

msgctxt "block:dropdown"
msgid "Verify score"
msgstr "Verifikasi perhitungan"

msgctxt "block:dropdown"
msgid "calculated"
msgstr "terhitung"

msgctxt "block:dropdown"
msgid "not calculated"
msgstr "tidak dihitung"

msgctxt "block:dropdown"
msgid "or"
msgstr "atau"
